$(document).ready(function () {
    var bounds  = new google.maps.LatLngBounds();
    var options = {
        zoom: 10,
        mapTypeControl: true,
        mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
        navigationControl: true,
        mapTypeId: google.maps.MapTypeId.MAP
    };
    var map            = new google.maps.Map(document.getElementById("map"), options);
    var polylineCoords = [];
    for (var x in data) {
        var point = new google.maps.LatLng(data[x]["lat"], data[x]["lon"]);
        polylineCoords.push(point);
        bounds.extend(point);
    }
    var polyline = new google.maps.Polyline({
        path: polylineCoords,
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 2,
        editable: true
    });

    map.fitBounds(bounds);
    polyline.setMap(map);
});

