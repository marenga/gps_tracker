<?php
/**
 * Created by PhpStorm.
 * User: ricma
 * Date: 25.2.17.
 * Time: 19.26
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Track;
use AppBundle\Entity\User;
use AppBundle\Form\TrackType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class TrackController
 */
class TrackController extends Controller
{
    /**
     * @Route("/", name="homepage")
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $tracks = $this->getDoctrine()->getRepository('AppBundle:Track')->findBy(['user' => $this->getUser()], ['tsi' => 'desc']);
        
        return $this->render('@FOSUser/index.html.twig', ['tracks' => $tracks]);
    }
    
    /**
     * @Route("/add-track", name="add_track")
     *
     * @return Response
     */
    public function addTrackAction(Request $request)
    {
        $user = $this->getUser();
        if (!$user instanceof User) {
            $this->addFlash('alert alert-danger', 'Unauthorized!');
            return $this->redirectToRoute('homepage');
        }
        
        $track = new Track();
        $form  = $this->createForm(TrackType::class, $track);
        $form->handleRequest($request);
    
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $file = $form['file']->getData();
                $this->get('app.track.file.validator')->validate($file);
                $filePath = $this->get('app.track.file.uploader')->upload($file);
                $track->setFile($filePath);
                $track->setUser($user);
                $em = $this->getDoctrine()->getManager();
                $em->persist($track);
                $em->flush();
            } catch (\Exception $e) {
                $this->addFlash('alert  alert-danger', $e->getMessage());
                return $this->render('@FOSUser/add_track.html.twig', ['form' => $form->createView()]);
            }
            $this->addFlash('alert  alert-success', 'Track added!');
            return $this->redirectToRoute('homepage');
        }
        
        return $this->render('@FOSUser/add_track.html.twig', ['form' => $form->createView()]);
    }
    
    /**
     * @Route("/delete-track/{trackId}", name="delete_track")
     *
     * @param integer $trackId Track id.
     *
     * @return Response
     */
    public function deleteAction(int $trackId)
    {
        $user = $this->getUser();
        if (!$user instanceof User) {
            $this->addFlash('alert  alert-danger', 'Unauthorized!');
            return $this->redirectToRoute('homepage');
        }
        
        $em    = $this->getDoctrine()->getManager();
        $track = $this->getDoctrine()->getRepository('AppBundle:Track')->findOneBy(['id' => $trackId, 'user' => $user->getId()]);
        if ($track instanceof Track) {
            $this->addFlash('alert  alert-success', 'Track "' . $track->getTitle() . '" deleted!');
            $em->remove($track);
            $em->flush();
        } else {
            $this->addFlash('alert  alert-danger', 'Track not found!');
        }
        
        return $this->redirectToRoute('homepage');
    }
    
    /**
     * @Route("/view-track/{trackId}", name="view_track")
     *
     * @param integer $trackId Track id.
     *
     * @return Response
     */
    public function viewAction(int $trackId)
    {
        $user = $this->getUser();
        if (!$user instanceof User) {
            $this->addFlash('alert  alert-danger', 'Unauthorized!');
            return $this->redirectToRoute('homepage');
        }
        
        $track = $this->getDoctrine()->getRepository('AppBundle:Track')->findOneBy(['id' => $trackId, 'user' => $user->getId()]);
        if (!$track instanceof Track) {
            $this->addFlash('alert  alert-danger', 'Track not found!');
            return $this->redirectToRoute('homepage');
        } else {
            $parsedData = $this->get('app.gpx.parser')->parse($track->getFile());
            $apiKey     = $this->getParameter('gps_tracker_gmap_api_key');
            
            return $this->render('@FOSUser/view_track.html.twig', [
                'api_key' => $apiKey,
                'data'    => json_encode($parsedData)
            ]);
        }
    }
}
