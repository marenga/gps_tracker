<?php
/**
 * Created by PhpStorm.
 * User: ricma
 * Date: 25.2.17.
 * Time: 08.55
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    
    public function __construct()
    {
        parent::__construct();
        
        $this->tracks = new ArrayCollection();
    }
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\OneToMany(targetEntity="Track", mappedBy="user")
     */
    private $tracks;
    
    /**
     * @return mixed
     */
    public function getTracks()
    {
        return $this->tracks;
    }
    
    /**
     * @param mixed $tracks
     */
    public function setTracks($tracks)
    {
        $this->tracks = $tracks;
    }
}