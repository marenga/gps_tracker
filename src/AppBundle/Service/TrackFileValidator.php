<?php
/**
 * Created by PhpStorm.
 * User: ricma
 * Date: 25.2.17.
 * Time: 21.14
 */

namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class TrackFileValidator
 */
class TrackFileValidator
{
    /**
     * @var array
     */
    private $allowedFileTypes;
    
    /**
     * TrackFileValidator constructor.
     * @param array $allowedFileTypes An array of allowed file types.
     */
    public function __construct(array $allowedFileTypes)
    {
        $this->allowedFileTypes = $allowedFileTypes;
    }
    
    /**
     * @param UploadedFile $file Uploaded file.
     * @throws \Exception
     *
     * @return void
     */
    public function validate(UploadedFile $file)
    {
        $ext = $file->guessExtension();
        if (!$ext || !in_array($ext, $this->allowedFileTypes)) {
            throw new \Exception('Invalid file type');
        }
    }
}
