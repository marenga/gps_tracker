<?php
/**
 * Created by PhpStorm.
 * User: ricma
 * Date: 25.2.17.
 * Time: 22.44
 */

namespace AppBundle\Service;

/**
 * Class GpxParser
 */
class GpxParser
{
    /**
     * @var array
     */
    private $parsed = [];
    
    /**
     * @param string $filePath Path to file to parse.
     *
     * @return array
     */
    public function parse(string $filePath)
    {
        $file = file_get_contents($filePath);
        $doc  = new \DOMDocument();
        $doc->loadXML($file);
        $tracks   = $doc->getElementsByTagName('trk');
        foreach ($tracks as $trackIndex => $track) {
            $segments = $track->getElementsByTagName('trkseg');
            foreach ($segments as $segmentIndex => $segment) {
                $trackPoints = $segment->getElementsByTagName('trkpt');
                foreach ($trackPoints as $trackPointIndex => $trackPoint) {
                    $ele      = $trackPoint->getElementsByTagName('ele')[0]->nodeValue;
                    $datetime = $trackPoint->getElementsByTagName('time')[0]->nodeValue;
                    $lat      = $trackPoint->getAttribute('lat');
                    $lon      = $trackPoint->getAttribute('lon');
                    $this->parsed[] = [
                        'ele'      => $ele,
                        'datetime' => $datetime,
                        'lat'      => $lat,
                        'lon'      => $lon
                    ];
                }
            }
        }
        
        return $this->parsed;
    }
}