<?php
/**
 * Created by PhpStorm.
 * User: ricma
 * Date: 25.2.17.
 * Time: 19.57
 */

namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class TrackFileUploader
 */
class TrackFileUploader
{
    /**
     * @var string
     */
    private $appPath;
    
    /**
     * @var string
     */
    private $uploadFolderPath;
    
    /**
     * TrackFileUploader constructor.
     * @param string $appPath          Absolute path to app/ folder.
     * @param string $uploadFolderPath Upload folder path.
     */
    public function __construct(string $appPath, string $uploadFolderPath)
    {
        $this->appPath          = $appPath;
        $this->uploadFolderPath = $uploadFolderPath;
    }
    
    /**
     * @param UploadedFile $file UploadedFile.
     *
     * @return string
     */
    public function upload(UploadedFile $file)
    {
        $varPath   = dirname($this->appPath) . '/' . $this->uploadFolderPath;
        $extension = $file->getExtension();
        if (!$extension) {
            $extension = '.gpx';
        }
        $newFilename = time() . $extension;
        $file->move($varPath, $newFilename);
        
        return $varPath . '/' . $newFilename;
    }
}